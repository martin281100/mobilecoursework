import React, { useEffect, useState } from "react";
import {
  Box,
  Heading,
  AspectRatio,
  Image,
  Text,
  Center,
  HStack,
  Stack,
  Button,
} from "native-base";
import { IRespondProperty } from "../../interfaces";
import { openMyDB } from "../../libs/openDatabase";
import { DeleteConfirmationModal } from "../confirmationModal/delete";
import { EditNotesModal } from "../confirmationModal/EditNotes";
import { EditModal } from "../confirmationModal/edit";

export const PropertyDetail = ({
  navigation,
  propertyId,
}: {
  navigation: any;
  propertyId: number;
}) => {
  const [data, setData] = useState<IRespondProperty>({
    notes: "",
    price: 10,
    reporter: "",
    createdAt: "",
    bedroom: 1,
    propertyType: "",
    furnitureOpt: "",
  });
  const [showModal, setShowModal] = useState(false);
  const [showNotesModal, setShowNotesModal] = useState(false);
  const [showUpdateModal, setShowUpdateModal] = useState(false);

  const deleteItem = () => {
    openMyDB.transaction(
      (tx) => {
        // tx.executeSql("ALTER TABLE property ADD createdAt TEXT");
        tx.executeSql(`DELETE FROM property WHERE id = ${propertyId}`, []);
      },
      (e) => {
        console.log("Delete Failed:", e.message);
      },
      () => {
        navigation.navigate("TabOne");
      }
    );
  };
  const updateNotes = (content: string) => {
    openMyDB.transaction(
      (tx) => {
        // tx.executeSql("ALTER TABLE property ADD createdAt TEXT");
        tx.executeSql(
          `UPDATE property SET notes = '${content}' WHERE id = ${propertyId}`,
          []
        );
      },
      (e) => {
        console.log("Update Failed:", e.message);
      },
      () => {
        setData({ ...data, notes: content });
        console.log("Update Success");
      }
    );
  };

  const onPressDelete = () => {
    setShowModal(true);
  };
  const onPressEditNotes = () => {
    setShowNotesModal(true);
  };
  const onPressEdit = () => {
    setShowUpdateModal(true);
  };
  useEffect(() => {
    openMyDB.transaction(
      (tx) => {
        tx.executeSql(
          `select * from property where id = ${propertyId}`,
          [],
          (_, { rows }) => setData(rows._array[0])
        );
      },
      (e) => {
        console.log("error", e);
        navigation.navigate("TabOne");
      }
    );
  }, [showUpdateModal]);
  return (
    <Box
      //   rounded="lg"
      overflow="hidden"
      height="full"
      width="full"
      display="flex"
      flexDirection="column"
      justifyContent="space-between"
      shadow={1}
      _light={{ backgroundColor: "gray.50" }}
      _dark={{ backgroundColor: "gray.700" }}
    >
      <Box>
        <AspectRatio ratio={16 / 9}>
          <Image
            source={{
              uri: "https://www.holidify.com/images/cmsuploads/compressed/Bangalore_citycover_20190613234056.jpg",
            }}
            alt="image"
          />
        </AspectRatio>
        <Center
          bg="violet.500"
          _text={{ color: "white", fontWeight: "700", fontSize: "xs" }}
          position="absolute"
          bottom={0}
          px="3"
          py="1.5"
        >
          PHOTOS
        </Center>
      </Box>
      <Stack p="4" space={3}>
        <Stack space={2}>
          <Heading size="md" ml="-1">
            {data.reporter}
          </Heading>
          <Text
            fontSize="xs"
            _light={{ color: "violet.500" }}
            _dark={{ color: "violet.300" }}
            fontWeight="500"
            ml="-0.5"
            mt="-1"
          >
            {data.propertyType} - {data.furnitureOpt}
          </Text>
        </Stack>
        <Text fontWeight="400">Price: ${data.price}</Text>
        <Text fontWeight="400">Notes: {data.notes}</Text>
        <Text
          fontWeight="400"
          color="gray.500"
          underline
          fontSize="xs"
          onPress={onPressEditNotes}
        >
          Edit notes
        </Text>

        <HStack alignItems="center" space={4} justifyContent="space-between">
          <HStack alignItems="center">
            <Text color="gray.500" fontWeight="400">
              {new Date(data.createdAt).toUTCString()}
            </Text>
          </HStack>
        </HStack>
        <Button color="cyan" onPress={onPressEdit}>
          Edit
        </Button>
        <Button colorScheme="danger" onPress={onPressDelete}>
          Delete
        </Button>
      </Stack>
      <DeleteConfirmationModal
        showModal={showModal}
        setShowModal={setShowModal}
        deleteData={deleteItem}
      />
      <EditNotesModal
        showModal={showNotesModal}
        setShowModal={setShowNotesModal}
        updateNotes={updateNotes}
        notesContent={data.notes ? data.notes : ""}
      />
      <EditModal
        showModal={showUpdateModal}
        setShowModal={setShowUpdateModal}
        propertyData={data}
      />
    </Box>
  );
};
