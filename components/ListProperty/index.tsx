import React, { useEffect, useState } from "react";
import {
  Box,
  FlatList,
  Heading,
  Avatar,
  HStack,
  VStack,
  Text,
  Spacer,
  Pressable,
  Icon,
  Input,
} from "native-base";
import { openMyDB } from "../../libs/openDatabase";
import { IRespondProperty } from "../../interfaces";
import { MaterialIcons } from "@expo/vector-icons";
export const ListProperty = ({
  navigation,
  route,
}: {
  navigation: any;
  route: any;
}) => {
  const [data, setData] = useState<IRespondProperty[]>([]);
  const [searchText, setSearchText] = useState("");
  const fetchData = () => {
    openMyDB.transaction((tx) => {
      // tx.executeSql("ALTER TABLE property ADD createdAt TEXT");
      tx.executeSql("select * from property", [], (_, { rows }) =>
        setData(rows._array)
      );
    });
  };

  const handleSelect = (id: number) => {
    navigation.navigate("Property Detail", { propertyId: id });
  };
  const handleOnSearch = () => {
    openMyDB.transaction((tx) => {
      // tx.executeSql("ALTER TABLE property ADD createdAt TEXT");
      tx.executeSql(
        `select * from property where propertyType like '%${searchText}%'`,
        [],
        (_, { rows }) => setData(rows._array)
      );
    });
  };

  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      fetchData();
    });

    return unsubscribe;
  }, [navigation]);

  return (
    <Box
      w={{
        base: "100%",
        md: "25%",
      }}
      h="100%"
    >
      <Heading fontSize="xl" p="4" pb="3">
        Available Property
      </Heading>
      <Input
        w={{
          base: "95%",
          md: "25%",
        }}
        onChangeText={(e) => setSearchText(e)}
        variant="outline"
        mx="auto"
        InputLeftElement={
          <Icon
            as={<MaterialIcons name="search" />}
            size={5}
            ml="2"
            color="muted.400"
          />
        }
        onSubmitEditing={() => {
          handleOnSearch();
        }}
        placeholder="Search Property Type"
      />
      <FlatList
        data={data}
        renderItem={({ item }: { item: IRespondProperty }) => (
          <Pressable
            onPress={() => {
              handleSelect(Number(item.id));
            }}
          >
            <Box
              borderBottomWidth="1"
              _dark={{
                borderColor: "gray.600",
              }}
              borderColor="coolGray.200"
              pl="4"
              pr="5"
              py="2"
            >
              <HStack space={3} justifyContent="space-between">
                <Avatar
                  size="48px"
                  source={{
                    uri: "https://i.pinimg.com/originals/38/d7/5b/38d75b985d9d08ce0959201f8198f405.jpg",
                  }}
                />
                <VStack>
                  <Text
                    _dark={{
                      color: "warmGray.50",
                    }}
                    color="coolGray.800"
                    bold
                  >
                    {item.propertyType}
                  </Text>
                  <Text
                    color="coolGray.600"
                    _dark={{
                      color: "warmGray.200",
                    }}
                  >
                    {item.reporter}
                  </Text>
                </VStack>
                <Spacer />
                <Text
                  fontSize="xs"
                  _dark={{
                    color: "warmGray.50",
                  }}
                  color="coolGray.800"
                  alignSelf="flex-start"
                >
                  {new Date(item.createdAt).toUTCString()}
                </Text>
              </HStack>
            </Box>
          </Pressable>
        )}
        keyExtractor={(item) => item.id}
      />
    </Box>
  );
};
