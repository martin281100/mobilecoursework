import React, { useState } from "react";
import {
  Button,
  CheckIcon,
  FormControl,
  Input,
  KeyboardAvoidingView,
  NumberInput,
  NumberInputField,
  ScrollView,
  Select,
  VStack,
  WarningOutlineIcon,
  Text,
  Box,
  View,
} from "native-base";
import { Platform } from "react-native";
import { ConfirmationModal } from "../confirmationModal";
import { PropertyService } from "../../services/PropertyService";
import DateTimePicker from "@react-native-community/datetimepicker";

export const CreateNewProperty = ({ navigation }: { navigation: any }) => {
  const [furnitureOpt, setFurnitureOpt] = useState("");
  const [bedroom, setBedroom] = useState(1);
  const [price, setPrice] = useState(10);
  const [propertyType, setPropertyType] = useState("");
  const [notes, setNotes] = useState("");
  const [reporter, setReporter] = useState("");
  const [isInvalid, setIsInvalid] = useState<any>({});
  const [errorMsg, setErrorMsg] = useState<any>({});
  const [showModal, setShowModal] = useState(false);
  const [error, setError] = useState(false);
  const [date, setDate] = useState(new Date());
  const [show, setShow] = useState(false);
  const service = new PropertyService();
  //Update state on fields change
  const showMode = () => {
    setShow(true);
  };
  const showDatepicker = () => {
    showMode();
  };

  const onChange = (event: any, selectedDate: any) => {
    const currentDate = selectedDate || date;
    setShow(Platform.OS === "ios");
    setDate(currentDate);
  };
  const handleChange = (key: string, value: string) => {
    switch (key) {
      case "bedroom":
        setBedroom(Number(value));
        break;
      case "price":
        setPrice(Number(value));
        break;
      case "propertyType":
        setPropertyType(value);
        break;
      case "notes":
        setNotes(value);
        break;
      case "reporter":
        setReporter(value);
        break;
      case "addDate":
        setAddDate(value);
      default:
        break;
    }
  };

  //Validate if blank field
  const validate = () => {
    let invalidObj = {};
    let msgObj = {};
    let isvalid = true;
    if (!bedroom) {
      invalidObj = { ...invalidObj, bedroom: true };
      msgObj = {
        ...msgObj,
        bedroom: "Bedroom quantity is higher than 0!",
      };
      isvalid = false;
    }
    if (!propertyType) {
      invalidObj = { ...invalidObj, propertyType: true };
      msgObj = {
        ...msgObj,
        propertyType: "Property Type is required!",
      };

      isvalid = false;
    }
    if (!price) {
      invalidObj = { ...invalidObj, price: true };
      msgObj = {
        ...msgObj,
        price: "Price is higher than 0!",
      };
      isvalid = false;
    }
    if (!reporter) {
      invalidObj = { ...invalidObj, reporter: true };
      msgObj = {
        ...msgObj,
        reporter: "Reporter name is required!",
      };
      isvalid = false;
    }
    setIsInvalid(invalidObj);
    setErrorMsg(msgObj);

    return isvalid;
  };

  const insertData = async () => {
    await service.insertProperty(
      propertyType,
      notes,
      reporter,
      price,
      bedroom,
      furnitureOpt,
      setError,
      navigation,
      date
    );
  };
  const handleSubmit = () => {
    setError(false);
    validate() ? setShowModal(true) : null;
  };
  return (
    <FormControl
      w={{
        base: "75%",
        md: "25%",
      }}
      // h="full"
      maxHeight="full"
      // mt={2}
    >
      <KeyboardAvoidingView
        // h={{
        //   base: "650px",
        //   lg: "auto",
        // }}
        behavior={Platform.OS === "ios" ? "padding" : "height"}
        keyboardVerticalOffset={40}
      >
        <ScrollView>
          <VStack p="6" flex="1" py="6" justifyContent="flex-end">
            <FormControl isInvalid={isInvalid.propertyType} isRequired>
              <FormControl.Label>Property type </FormControl.Label>
              <Input
                placeholder="Property"
                py={2}
                onChangeText={(e) => {
                  handleChange("propertyType", e);
                }}
              />
              <FormControl.ErrorMessage
                leftIcon={<WarningOutlineIcon size="xs" />}
              >
                {errorMsg.propertyType}
              </FormControl.ErrorMessage>
            </FormControl>

            <FormControl isInvalid={isInvalid.bedroom} isRequired>
              <FormControl.Label>Bedrooms </FormControl.Label>
              <NumberInput defaultValue={bedroom.toString()} min={1}>
                <NumberInputField
                  py={2}
                  onChange={(e) => {
                    handleChange("bedroom", e.nativeEvent.text);
                  }}
                />
              </NumberInput>
              <FormControl.ErrorMessage
                leftIcon={<WarningOutlineIcon size="xs" />}
              >
                {errorMsg.bedroom}
              </FormControl.ErrorMessage>
            </FormControl>

            <FormControl isInvalid={isInvalid.price} isRequired>
              <FormControl.Label>
                Price per month (US Dollars){" "}
              </FormControl.Label>
              <NumberInput defaultValue={price.toString()}>
                <NumberInputField
                  py={2}
                  onChange={(e) => {
                    handleChange("price", e.nativeEvent.text);
                  }}
                />
              </NumberInput>
              <FormControl.ErrorMessage
                leftIcon={<WarningOutlineIcon size="xs" />}
              >
                {errorMsg.price}
              </FormControl.ErrorMessage>
            </FormControl>
            <FormControl>
              <FormControl.Label>Furniture </FormControl.Label>
              <VStack alignItems="center">
                <Select
                  selectedValue={furnitureOpt}
                  w="full"
                  accessibilityLabel="Choose Service"
                  placeholder="Choose Service"
                  _selectedItem={{
                    bg: "teal.200",
                    endIcon: <CheckIcon size="5" />,
                  }}
                  mt={1}
                  onValueChange={(itemValue) => setFurnitureOpt(itemValue)}
                >
                  <Select.Item label="Furnished" value="furnished" />
                  <Select.Item label="Unfurnished" value="unfurnished" />
                  <Select.Item label="Part Furnished" value="part" />
                </Select>
              </VStack>
            </FormControl>
            <FormControl mt={5}>
              <FormControl.Label>Notes </FormControl.Label>
              <Input
                placeholder=""
                onChangeText={(e) => {
                  handleChange("notes", e);
                }}
              />
            </FormControl>
            <FormControl mt="3" isRequired>
              <FormControl.Label>Add Time </FormControl.Label>
              <Box>
                <View display="flex" flexDirection="row">
                  <Text onPress={showDatepicker} underline color="cyan.500">
                    {date.toUTCString()}
                  </Text>
                </View>
                {show && (
                  <DateTimePicker
                    testID="dateTimePicker"
                    value={date}
                    mode="date"
                    is24Hour={true}
                    display="default"
                    onChange={onChange}
                  />
                )}
              </Box>
            </FormControl>
            <FormControl mt={5} isInvalid={isInvalid.reporter} isRequired>
              <FormControl.Label>Reporter Name </FormControl.Label>
              <Input
                placeholder="Enter reporter name"
                onChangeText={(e) => {
                  handleChange("reporter", e);
                }}
              />
              <FormControl.ErrorMessage
                leftIcon={<WarningOutlineIcon size="xs" />}
              >
                {errorMsg.reporter}
              </FormControl.ErrorMessage>
            </FormControl>
            {error ? (
              <Text fontSize="sm" color="red.500">
                Property type existed!
              </Text>
            ) : null}

            <Button mt="5" onPress={() => handleSubmit()}>
              Create
            </Button>
          </VStack>
        </ScrollView>
      </KeyboardAvoidingView>
      <ConfirmationModal
        data={{
          bedroom,
          reporter,
          furnitureOpt,
          price,
          propertyType,
          notes,
          date,
        }}
        showModal={showModal}
        setShowModal={setShowModal}
        insertData={insertData}
      />
    </FormControl>
  );
};
