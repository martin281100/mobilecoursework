import React from "react";
import {
  Button,
  Modal,
  FormControl,
  Input,
  Center,
  NativeBaseProvider,
  Text,
  Box,
  Heading,
} from "native-base";
export const DeleteConfirmationModal = ({
  showModal,
  setShowModal,
  deleteData,
}: {
  showModal: boolean;
  setShowModal: React.Dispatch<React.SetStateAction<boolean>>;
  deleteData: () => void;
}) => {
  const handleSubmit = () => {
    deleteData();
  };
  return (
    <>
      <Modal isOpen={showModal} onClose={() => setShowModal(false)}>
        <Modal.Content maxWidth="400px">
          <Modal.CloseButton />
          <Modal.Header>
            <Heading size="md">Are you sure?</Heading>
          </Modal.Header>
          <Modal.Body>
            <Text fontSize="lg">Do you want to delete this property ?</Text>
          </Modal.Body>
          <Modal.Footer>
            <Button.Group space={2}>
              <Button
                variant="ghost"
                colorScheme="blueGray"
                onPress={() => {
                  setShowModal(false);
                }}
              >
                Cancel
              </Button>
              <Button
                onPress={() => {
                  handleSubmit();
                  setShowModal(false);
                }}
                colorScheme="danger"
              >
                Delete
              </Button>
            </Button.Group>
          </Modal.Footer>
        </Modal.Content>
      </Modal>
    </>
  );
};
