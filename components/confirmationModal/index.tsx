import React from "react";
import {
  Button,
  Modal,
  FormControl,
  Input,
  Center,
  NativeBaseProvider,
  Text,
  Box,
  Heading,
} from "native-base";
export const ConfirmationModal = ({
  data,
  showModal,
  setShowModal,
  insertData,
}: {
  data: {
    bedroom: number;
    reporter: string;
    furnitureOpt: string;
    price: number;
    propertyType: string;
    notes: string;
    date: Date;
  };
  showModal: boolean;
  setShowModal: React.Dispatch<React.SetStateAction<boolean>>;
  insertData: () => void;
}) => {
  const handleSubmit = () => {
    insertData();
  };
  return (
    <>
      <Modal isOpen={showModal} onClose={() => setShowModal(false)}>
        <Modal.Content maxWidth="400px">
          <Modal.CloseButton />
          <Modal.Header>
            <Heading size="md">Are you sure?</Heading>
          </Modal.Header>
          <Modal.Body>
            <Text fontSize="lg">Do you want to add this property ?</Text>
            <Box mx="3" mt="2">
              <Text fontSize="sm">Property Type: {data.propertyType}</Text>
              <Text fontSize="sm">Bedroom: {data.bedroom}</Text>
              <Text fontSize="sm">Price: {data.price}</Text>
              {data.furnitureOpt ? (
                <Text fontSize="sm">
                  Furniture Options: {data.furnitureOpt}
                </Text>
              ) : null}
              {data.notes ? (
                <Text fontSize="sm">Notes: {data.notes}</Text>
              ) : null}
              <Text fontSize="sm">Reporter: {data.reporter}</Text>
              <Text fontSize="sm">Add Time: {data.date.toUTCString()}</Text>
            </Box>
          </Modal.Body>
          <Modal.Footer>
            <Button.Group space={2}>
              <Button
                variant="ghost"
                colorScheme="blueGray"
                onPress={() => {
                  setShowModal(false);
                }}
              >
                Cancel
              </Button>
              <Button
                onPress={() => {
                  handleSubmit();
                  setShowModal(false);
                }}
              >
                Save
              </Button>
            </Button.Group>
          </Modal.Footer>
        </Modal.Content>
      </Modal>
    </>
  );
};
