import React, { useEffect, useState } from "react";
import {
  Button,
  Modal,
  Heading,
  Input,
  FormControl,
  ScrollView,
  KeyboardAvoidingView,
  VStack,
  WarningOutlineIcon,
  NumberInput,
  NumberInputField,
  Select,
  CheckIcon,
} from "native-base";
import { openMyDB } from "../../libs/openDatabase";
import { Platform } from "react-native";
import { IRespondProperty } from "../../interfaces";
export const EditModal = ({
  showModal,
  setShowModal,
  propertyData,
}: {
  showModal: boolean;
  setShowModal: React.Dispatch<React.SetStateAction<boolean>>;
  propertyData: IRespondProperty;
}) => {
  const [furnitureOpt, setFurnitureOpt] = useState("");
  const [bedroom, setBedroom] = useState(1);
  const [price, setPrice] = useState(10);
  const [propertyType, setPropertyType] = useState("");
  const [notes, setNotes] = useState("");
  const [reporter, setReporter] = useState("");
  const [isInvalid, setIsInvalid] = useState<any>({});
  const [errorMsg, setErrorMsg] = useState<any>({});

  const updateData = () => {
    openMyDB.transaction(
      (tx) => {
        // tx.executeSql("ALTER TABLE property ADD createdAt TEXT");
        tx.executeSql(
          `UPDATE property SET notes = '${notes}', furnitureOpt = '${furnitureOpt}', bedroom = '${bedroom}', reporter = '${reporter}', price = '${price}', propertyType = '${propertyType}' WHERE id = ${propertyData.id}`,
          []
        );
      },
      (e) => {
        console.log("Update Failed:", e.message);
      },
      () => {
        setShowModal(false);
        console.log("Update Success");
      }
    );
  };
  //Update state on fields change
  const handleChange = (key: string, value: string) => {
    switch (key) {
      case "bedroom":
        setBedroom(Number(value));
        break;
      case "price":
        setPrice(Number(value));
        break;
      case "propertyType":
        setPropertyType(value);
        break;
      case "notes":
        setNotes(value);
        break;
      case "reporter":
        setReporter(value);
        break;
      default:
        break;
    }
  };

  //Validate if blank field
  const validate = () => {
    let invalidObj = {};
    let msgObj = {};
    let isvalid = true;
    if (!bedroom) {
      invalidObj = { ...invalidObj, bedroom: true };
      msgObj = {
        ...msgObj,
        bedroom: "Bedroom quantity is higher than 0!",
      };
      isvalid = false;
    }
    if (!propertyType) {
      invalidObj = { ...invalidObj, propertyType: true };
      msgObj = {
        ...msgObj,
        propertyType: "Property Type is required!",
      };

      isvalid = false;
    }
    if (!price) {
      invalidObj = { ...invalidObj, price: true };
      msgObj = {
        ...msgObj,
        price: "Price is higher than 0!",
      };
      isvalid = false;
    }
    if (!reporter) {
      invalidObj = { ...invalidObj, reporter: true };
      msgObj = {
        ...msgObj,
        reporter: "Reporter name is required!",
      };
      isvalid = false;
    }
    setIsInvalid(invalidObj);
    setErrorMsg(msgObj);

    return isvalid;
  };

  useEffect(() => {
    setFurnitureOpt(propertyData.furnitureOpt);
    setBedroom(propertyData.bedroom);
    setPrice(propertyData.price);
    setPropertyType(propertyData.propertyType);
    setNotes(propertyData.notes ? propertyData.notes : "");
    setReporter(propertyData.reporter);
  }, [propertyData]);
  const handleSubmit = () => {
    validate() ? updateData() : null;
  };
  return (
    <>
      <Modal isOpen={showModal} onClose={() => setShowModal(false)}>
        <Modal.Content maxWidth="400px">
          <Modal.CloseButton />
          <Modal.Header>
            <Heading size="md">Edit Property</Heading>
          </Modal.Header>
          <Modal.Body>
            <FormControl maxHeight="full" mt={5}>
              <KeyboardAvoidingView
                behavior={Platform.OS === "ios" ? "padding" : "height"}
                keyboardVerticalOffset={40}
              >
                <ScrollView>
                  <VStack p="6" flex="1" py="6" justifyContent="flex-end">
                    <FormControl isInvalid={isInvalid.propertyType} isRequired>
                      <FormControl.Label>Property type</FormControl.Label>
                      <Input
                        placeholder="Property"
                        py={2}
                        onChangeText={(e) => {
                          handleChange("propertyType", e);
                        }}
                        defaultValue={propertyType}
                      />
                      <FormControl.ErrorMessage
                        leftIcon={<WarningOutlineIcon size="xs" />}
                      >
                        {errorMsg.propertyType}
                      </FormControl.ErrorMessage>
                    </FormControl>

                    <FormControl isInvalid={isInvalid.bedroom} isRequired>
                      <FormControl.Label>Bedrooms </FormControl.Label>
                      <NumberInput defaultValue={bedroom.toString()} min={1}>
                        <NumberInputField
                          py={2}
                          onChange={(e) => {
                            handleChange("bedroom", e.nativeEvent.text);
                          }}
                        />
                      </NumberInput>
                      <FormControl.ErrorMessage
                        leftIcon={<WarningOutlineIcon size="xs" />}
                      >
                        {errorMsg.bedroom}
                      </FormControl.ErrorMessage>
                    </FormControl>

                    <FormControl isInvalid={isInvalid.price} isRequired>
                      <FormControl.Label>
                        Price per month (US Dollars){" "}
                      </FormControl.Label>
                      <NumberInput defaultValue={price.toString()}>
                        <NumberInputField
                          py={2}
                          onChange={(e) => {
                            handleChange("price", e.nativeEvent.text);
                          }}
                        />
                      </NumberInput>
                      <FormControl.ErrorMessage
                        leftIcon={<WarningOutlineIcon size="xs" />}
                      >
                        {errorMsg.price}
                      </FormControl.ErrorMessage>
                    </FormControl>
                    <FormControl>
                      <FormControl.Label>Furniture </FormControl.Label>
                      <VStack alignItems="center">
                        <Select
                          selectedValue={furnitureOpt}
                          w="full"
                          accessibilityLabel="Choose Service"
                          placeholder="Choose Service"
                          _selectedItem={{
                            bg: "teal.200",
                            endIcon: <CheckIcon size="5" />,
                          }}
                          mt={1}
                          onValueChange={(itemValue) =>
                            setFurnitureOpt(itemValue)
                          }
                          defaultValue={furnitureOpt}
                        >
                          <Select.Item label="Furnished" value="furnished" />
                          <Select.Item
                            label="Unfurnished"
                            value="unfurnished"
                          />
                          <Select.Item label="Part Furnished" value="part" />
                        </Select>
                      </VStack>
                    </FormControl>
                    <FormControl mt={5}>
                      <FormControl.Label>Notes </FormControl.Label>
                      <Input
                        placeholder=""
                        onChangeText={(e) => {
                          handleChange("notes", e);
                        }}
                        defaultValue={notes}
                      />
                    </FormControl>
                    <FormControl
                      mt={5}
                      isInvalid={isInvalid.reporter}
                      isRequired
                    >
                      <FormControl.Label>Reporter Name </FormControl.Label>
                      <Input
                        placeholder="Enter reporter name"
                        onChangeText={(e) => {
                          handleChange("reporter", e);
                        }}
                        defaultValue={reporter}
                      />
                      <FormControl.ErrorMessage
                        leftIcon={<WarningOutlineIcon size="xs" />}
                      >
                        {errorMsg.reporter}
                      </FormControl.ErrorMessage>
                    </FormControl>
                  </VStack>
                </ScrollView>
              </KeyboardAvoidingView>
            </FormControl>
          </Modal.Body>
          <Modal.Footer>
            <Button.Group space={2}>
              <Button
                variant="ghost"
                colorScheme="blueGray"
                onPress={() => {
                  setShowModal(false);
                }}
              >
                Cancel
              </Button>
              <Button
                onPress={() => {
                  handleSubmit();
                  setShowModal(false);
                }}
                colorScheme="primary"
              >
                Update
              </Button>
            </Button.Group>
          </Modal.Footer>
        </Modal.Content>
      </Modal>
    </>
  );
};
