import React, { useEffect, useState } from "react";
import { Button, Modal, Text, Heading, TextArea, Input } from "native-base";
import { openMyDB } from "../../libs/openDatabase";
export const EditNotesModal = ({
  showModal,
  setShowModal,
  updateNotes,
  notesContent,
}: {
  showModal: boolean;
  setShowModal: React.Dispatch<React.SetStateAction<boolean>>;
  updateNotes: (content: string) => void;
  notesContent: string;
}) => {
  const [data, setData] = useState("");
  const handleSubmit = () => {
    updateNotes(data);
  };

  useEffect(() => {
    setData(notesContent);
  }, [notesContent]);
  return (
    <>
      <Modal isOpen={showModal} onClose={() => setShowModal(false)}>
        <Modal.Content maxWidth="400px">
          <Modal.CloseButton />
          <Modal.Header>
            <Heading size="md">Edit Notes</Heading>
          </Modal.Header>
          <Modal.Body>
            <Input
              placeholder="Enter notes here"
              defaultValue={data}
              onChangeText={(e) => {
                setData(e);
              }}
            />
          </Modal.Body>
          <Modal.Footer>
            <Button.Group space={2}>
              <Button
                variant="ghost"
                colorScheme="blueGray"
                onPress={() => {
                  setShowModal(false);
                }}
              >
                Cancel
              </Button>
              <Button
                onPress={() => {
                  handleSubmit();
                  setShowModal(false);
                }}
                colorScheme="primary"
              >
                Update
              </Button>
            </Button.Group>
          </Modal.Footer>
        </Modal.Content>
      </Modal>
    </>
  );
};
