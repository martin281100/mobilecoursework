import { openMyDB } from "../libs/openDatabase";

export class PropertyService {
  async insertProperty(
    propertyType: string,
    notes: string,
    reporter: string,
    price: number,
    bedroom: number,
    furnitureOpt: string,
    setError: React.Dispatch<React.SetStateAction<boolean>>,
    navigation: any,
    date: Date
  ) {
    openMyDB.transaction(
      (tx) => {
        tx.executeSql(
          `INSERT INTO property (propertyType, notes, reporter, price, bedroom, furnitureOpt, createdAt, addTime) values ('${propertyType}','${notes}','${reporter}',${price},${bedroom},'${furnitureOpt}','${new Date().toISOString()}','${date.toUTCString()}')`
        );

        tx.executeSql("select * from property", [], (_, { rows }) =>
          console.log(JSON.stringify(rows))
        );
      },
      (e) => {
        console.log("error", e.message);
        setError(true);
      },
      () => {
        navigation.navigate("TabOne");
      }
    );
  }
}
