To start this app, you have to follow these step:

- Download this project: https://gitlab.com/martin281100/mobilecoursework/-/archive/master/mobilecoursework-master.zip
- Install Nodejs
- Install yarn by run npm i -g yarn
- Go to this directory
- Run yarn install
- To start app, run yarn start
