import { StatusBar } from "expo-status-bar";
import { NativeBaseProvider } from "native-base";
import React, { useEffect } from "react";
import { SafeAreaProvider } from "react-native-safe-area-context";

import useCachedResources from "./hooks/useCachedResources";
import useColorScheme from "./hooks/useColorScheme";
import { openMyDB } from "./libs/openDatabase";
import Navigation from "./navigation";

export default function App() {
  const isLoadingComplete = useCachedResources();
  const colorScheme = useColorScheme();

  useEffect(() => {
    openMyDB.transaction((tx) => {
      tx.executeSql(
        "CREATE TABLE IF NOT EXISTS property (id INTEGER PRIMARY KEY AUTOINCREMENT, propertyType TEXT, notes TEXT, reporter TEXT, price FLOAT, bedroom INT, furnitureOpt TEXT, createdAt TEXT, addTime TEXT, UNIQUE(propertyType))"
      );
      // tx.executeSql("ALTER TABLE property ADD createdAt TEXT");
      tx.executeSql("select * from property", [], (_, { rows }) =>
        console.log(JSON.stringify(rows))
      );
    });
  });

  if (!isLoadingComplete) {
    return null;
  } else {
    return (
      <NativeBaseProvider>
        <SafeAreaProvider>
          <Navigation colorScheme={colorScheme} />
          <StatusBar />
        </SafeAreaProvider>
      </NativeBaseProvider>
    );
  }
}
