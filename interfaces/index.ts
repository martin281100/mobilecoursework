export interface IRequestProperty {
  propertyType: string;
  bedroom: number;
  price: number;
  furnitureType?: string;
  notes?: string;
  reporter: string;
}
export interface IRespondProperty extends IRequestProperty {
  furnitureOpt: string;
  createdAt: string;
  id?: number;
}
