import React from "react";
import { Center, NativeBaseProvider } from "native-base";
import { CreateNewProperty } from "../components/CreateNewProperty";

export default ({ navigation }: { navigation: any }) => {
  return (
    <NativeBaseProvider>
      <CreateNewProperty navigation={navigation} />
    </NativeBaseProvider>
  );
};
