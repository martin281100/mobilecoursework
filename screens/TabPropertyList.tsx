import React from "react";
import { Center, NativeBaseProvider } from "native-base";
import { ListProperty } from "../components/ListProperty";

export default ({ navigation, route }: { navigation: any; route: any }) => {
  return (
    <NativeBaseProvider>
      <Center>
        <ListProperty navigation={navigation} route={route} />
      </Center>
    </NativeBaseProvider>
  );
};
