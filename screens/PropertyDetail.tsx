import React from "react";
import { Center, NativeBaseProvider } from "native-base";
import { PropertyDetail } from "../components/PropertyDetail";

export default ({ navigation, route }: { navigation: any; route: any }) => {
  const propertyId = route.params.propertyId;

  return (
    <NativeBaseProvider>
      <Center>
        <PropertyDetail propertyId={propertyId} navigation={navigation} />
      </Center>
    </NativeBaseProvider>
  );
};
